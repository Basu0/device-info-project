package com.example.deviceinfoproject;

import android.widget.Filter;

import java.util.ArrayList;

public class custom_filtar extends Filter {
    adaptar adaptar;
    ArrayList<Model> fillterlist;

    public custom_filtar( ArrayList<Model> fillter,com.example.deviceinfoproject.adaptar adaptar) {
        this.adaptar = adaptar;
        this.fillterlist = fillter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults=new FilterResults();
        if (constraint!=null &&constraint.length()>0){

            constraint=constraint.toString().toUpperCase();
            ArrayList<Model>filtermodel=new ArrayList<>();
            for (int i=0;i<fillterlist.size();i++){

                if (fillterlist.get(i).getTitel().toUpperCase().contains(constraint)){

                  filtermodel.add(fillterlist.get(i));

                }
            }
           filterResults.count=filtermodel.size();
            filterResults.values=filtermodel;
        }
        else {

            filterResults.count=fillterlist.size();
            filterResults.values=fillterlist;
        }
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adaptar.models=(ArrayList<Model>) results.values;
        adaptar.notifyDataSetChanged();

    }
}
