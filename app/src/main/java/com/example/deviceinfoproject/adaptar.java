package com.example.deviceinfoproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Formattable;
import java.util.Formatter;

public class adaptar extends RecyclerView.Adapter<myHolder> implements Filterable {
    Context c;
    ArrayList<Model> models,filterlist;
    custom_filtar filtar;

    public adaptar(Context c, ArrayList<Model> models) {
        this.c = c;
        this.models = models;
        this.filterlist=models;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.model,null);
        return new myHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holdar, int i) {
        holdar.textView.setText( models.get(i).getTitel());
        holdar.description.setText( models.get(i).getDescription());
        holdar.imageView.setImageResource( models.get(i).getImg());

        Animation animation= AnimationUtils.loadAnimation(c,android.R.anim.slide_in_left);
        holdar.itemView.startAnimation(animation);

    }

    @Override
    public int getItemCount() {
        return models.size();
    }


    @Override
    public Filter getFilter() {
        if (filtar==null){
            filtar=new custom_filtar(filterlist,this);


        }
        return filtar;
    }
}
