package com.example.deviceinfoproject;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class myHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView textView,description;

    public myHolder(@NonNull View itemView) {
        super(itemView);
        this.imageView=itemView.findViewById(R.id.modelimagev);
        this.textView=itemView.findViewById(R.id.modelTextv);
        this.description=itemView.findViewById(R.id.modeldetails);
    }
}
