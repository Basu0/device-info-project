package com.example.deviceinfoproject;

import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class cardview_home extends AppCompatActivity {
    RecyclerView recyclerView;
    adaptar adaptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardview_home);
        recyclerView=(RecyclerView)findViewById(R.id.recycl_id);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        adaptar=new adaptar(this,getPlayers());
        recyclerView.setAdapter(adaptar);
    }
    private ArrayList<Model> getPlayers(){
    ArrayList<Model> models=new ArrayList<>();


    Model p=new Model();
    p.setTitel("Battery");
    p.setDescription("Battery Sample Descripration");
    p.setImg(R.drawable.battery);
    models.add(p);

        p=new Model();
        p.setTitel("System Apps");
        p.setDescription("System Apps Sample Descripration");
        p.setImg(R.drawable.systaml);

        p=new Model();
        p.setTitel("CPU");
        p.setDescription("CPU Sample Descripration");
        p.setImg(R.drawable.cpu);
        models.add(p);

        p=new Model();
        p.setTitel("Device");
        p.setDescription("DEVICE Sample Descripration");
        p.setImg(R.drawable.device);
        models.add(p);

        p=new Model();
        p.setTitel("Display");
        p.setDescription("Display Sample Descripration");
        p.setImg(R.drawable.display);
        models.add(p);

        p=new Model();
        p.setTitel("General");
        p.setDescription("General Sample Descripration");
        p.setImg(R.drawable.genarl_info);
        models.add(p);

        p=new Model();
        p.setTitel("Memory");
        p.setDescription("Memory Sample Descripration");
        p.setImg(R.drawable.memory);
        models.add(p);

        p=new Model();
        p.setTitel("Sensors");
        p.setDescription("Sensors Sample Descripration");
        p.setImg(R.drawable.sencor);
        models.add(p);

        p=new Model();
        p.setTitel("SIM");
        p.setDescription("SIM Sample Descripration");
        p.setImg(R.drawable.sim);
        models.add(p);



        p=new Model();
        p.setTitel("User Apps");
        p.setDescription("User Apps Sample Descripration");
        p.setImg(R.drawable.user);
    return models;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        MenuItem item=menu.findItem(R.id.search_id);
        SearchView searchView=(SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adaptar.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adaptar.getFilter().filter(s);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        //if (id==R.id.search_id){}
        return super.onOptionsItemSelected(item);
    }
}
